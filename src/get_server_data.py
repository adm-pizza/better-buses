#!/usr/bin/env python3

import os
import requests
from dotenv import load_dotenv

api_url = "https://api.nationaltransport.ie/gtfsr/v2/TripUpdates?format=json"

load_dotenv()
key = os.environ.get("API_KEY")

headers={"x-api-key":key}


x = requests.get(api_url, headers=headers)

print(x.status_code)
print(x.reason)
print(x.json())

