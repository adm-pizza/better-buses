import React, {useState, useEffect} from 'react';


import './App.css';

const bustimes = [
	{number :"39A", arrival: 65},
	{number :"16", arrival: 145},
	{number :"39", arrival: 278},
	{number :"39A", arrival: 400},
	{number :"145", arrival: 833},
]


function App() {
  	return (
    	<div className="App">
    	<BusVisuals />
      	<BusPosts />
    	</div>
  	);
}

function BusPosts() {
	const listTimes = bustimes.map(bustime =>
		<tr
			key={bustime.arrival}
		>
			<IncomingBus bus={bustime} />
		</tr>
	);
	return (
		<table>{listTimes}</table>
	);
}

function BusVisuals() {
	const listOfBuses = bustimes.map(bustime =>
		<>
		<BusLineTime time={Math.round(bustime.arrival/60)}mins/>
		<BusIcon number={bustime.number}/>
		</>
	);
	return(
		<div>
		<StopIcon />
		{listOfBuses}
		</div>
	);
}

function StopIcon() {
	return(
		<>
			🙂
		</>
	);
}

function BusLineTime({time}) {
	return(
		<span>
		- {time} - 
		</span>
	);
}

function BusIcon({number}) {
	return (
		<span>
			🚌{number}
		</span>
	);
}

function IncomingBus({bus}) {
	return (
		<>
			<th className="busnumber">{bus.number}</th>
			<th className="busarrival">{Math.round(bus.arrival/60)}mins</th>
		</>
	)
}

export default App;
